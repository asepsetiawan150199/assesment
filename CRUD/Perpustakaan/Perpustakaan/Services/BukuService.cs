﻿using Microsoft.AspNetCore.Server.HttpSys;
using Microsoft.EntityFrameworkCore.Migrations.Operations.Builders;
using Newtonsoft.Json;
using Perpustakaan.datamodels;
using Perpustakaan.viewmodels;
using System.Text;

namespace Perpustakaan.Services
{
    public class BukuService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public BukuService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<MBuku>> GetAllData()
        {
            List<MBuku> data = new List<MBuku>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "Buku/GetAllData");
            data = JsonConvert.DeserializeObject<List<MBuku>>(apiResponse);

            return data;

        }

        public async Task<VMResponse> Create(MBuku dataParam)
        {
            //Proses convert dari objek ke string format json
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu dikirim sebagai requast bofy
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil api dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "Buku/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari api
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari api ke objek
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;

        }
        public async Task<bool> CheckCategoryByName(string nameCategory, int id)
        {
            string apiRespon = await client.GetStringAsync(RouteAPI + $"Buku/CheckCategoryByName/{nameCategory}/{id}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiRespon);

            return isExist;
        }

        public async Task<MBuku> GetDataById(int id)
        {
            MBuku data = new MBuku();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"Buku/GetDataById/{id}"); 
            data = JsonConvert.DeserializeObject<MBuku>(apiResponse); 
            return data;
        }

        public async Task<VMResponse> Edit(MBuku dataParam)
        {
            string json = JsonConvert.SerializeObject(dataParam);

            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            var request = await client.PutAsync(RouteAPI + "Buku/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari api
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari api ke objek
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;

        }

        public async Task<VMResponse> Delete(int id, int createBy)
        {
            var request = await client.DeleteAsync(RouteAPI + $"Buku/Delete/{id}/{createBy}");

            if (request.IsSuccessStatusCode)
            {

                // ini adalah proses baca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari API ke Objek
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }


    }
}
