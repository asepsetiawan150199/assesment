﻿using Newtonsoft.Json;
using Perpustakaan.datamodels;
using Perpustakaan.viewmodels;
using System.Text;

namespace Perpustakaan.Services
{
    public class AnggotaService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public AnggotaService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }
        public async Task<List<MAnggotum>> GetAllData()
        {
            List<MAnggotum> data = new List<MAnggotum>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "Anggota/GetAllData");
            data = JsonConvert.DeserializeObject<List<MAnggotum>>(apiResponse);

            return data;

        }
        public async Task<VMResponse> Create(MAnggotum dataParam)
        {
            //Proses convert dari objek ke string format json
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu dikirim sebagai requast bofy
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil api dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "Anggota/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari api
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari api ke objek
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;

        }
        public async Task<bool> CheckName(string name, int id)
        {
            string apiRespon = await client.GetStringAsync(RouteAPI + $"Anggota/CheckName/{name}/{id}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiRespon);

            return isExist;
        }

        public async Task<MAnggotum> GetDataById(int id)
        {
            MAnggotum data = new MAnggotum();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"Anggota/GetDataById/{id}"); //get url API
            data = JsonConvert.DeserializeObject<MAnggotum>(apiResponse); //deserialis mengubah json menjadi objek, serialis sebaliknya
            return data;
        }

        public async Task<VMResponse> Edit(MAnggotum dataParam)
        {
            //Proses convert dari objek ke string format json
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu dikirim sebagai requast bofy
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil api dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "Anggota/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari api
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari api ke objek
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;

        }

        public async Task<VMResponse> Delete(int id, int modifedby)
        {
            var request = await client.DeleteAsync(RouteAPI + $"Anggota/Delete/{id}/{modifedby}");

            if (request.IsSuccessStatusCode)
            {

                // ini adalah proses baca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari API ke Objek
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }
    }
}
