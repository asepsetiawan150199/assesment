﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Perpustakaan.datamodels;
using Perpustakaan.Services;
using Perpustakaan.viewmodels;
using System.Data;
namespace xpos334.Controllers
{
    public class BukuController : Controller
    {
        private BukuService bukuService;
        private int IdUser = 1;

        public BukuController(BukuService _bukuService)
        {
            bukuService = _bukuService;
        }

        public async Task<IActionResult>          
            Index(string sortOrder,
                  string searchString,
                  string currentFilter,
                  int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            List<MBuku> data = await bukuService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Judul.ToLower().Contains(searchString.ToLower())
                || a.Penerbit.ToLower().Contains(searchString.ToLower())).ToList();
            }
            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Judul).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Judul).ToList();
                    break;
            }

            return View(PaginatedList<MBuku>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public IActionResult Create()
        {
            MBuku data = new MBuku();
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Create(MBuku dataParam)
        {
            dataParam.CreatedBy= IdUser;
            VMResponse respon = await bukuService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<JsonResult> CheckNameIsExsist(string nameCategory, int id)
        {
            bool isExist = await bukuService.CheckCategoryByName(nameCategory, id);
            return Json(isExist);
        }

        public async Task<IActionResult> Edit(int id)
        {
            MBuku data = await bukuService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(MBuku dataParam)
        {
            dataParam.ModifiedBy = IdUser;
            VMResponse respon = await bukuService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Detail(int id)
        {
            MBuku data = await bukuService.GetDataById(id);
            return PartialView(data);
        }

        /*      [HttpGet("HapusData")]*/
        public async Task<IActionResult> Delete(int id)
        {
            MBuku data = await bukuService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            int createBy = IdUser;
            VMResponse respon = await bukuService.Delete(id, createBy);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return RedirectToAction("Index");
        }
    }
}
