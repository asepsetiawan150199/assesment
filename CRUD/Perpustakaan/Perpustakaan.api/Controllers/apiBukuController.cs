﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Perpustakaan.datamodels;
using Perpustakaan.viewmodels;
namespace xpos334.api.Controllers
{
    [Route("Buku")] //bisa custom controller
    [ApiController]
    public class apiCategoryController : ControllerBase
    {
        private readonly PerpustakaanContext db;
        private VMResponse respon = new VMResponse();
        private int Iduser = 1;

        public apiCategoryController(PerpustakaanContext _db)
        {
            db = _db;
        }

        [HttpGet("GetAllData")]

        public List<MBuku> GetAllData()
        {
            List<MBuku> data = db.MBukus.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]

        public MBuku DataById(int id)
        {
            MBuku result = db.MBukus.Where(a => a.Id == id).FirstOrDefault();
            return result;
        }

        [HttpGet("CheckCategoryByName/{name}/{id}")]

        public bool CheckName(string name, int id)
        {
            MBuku data = new MBuku();

            if (id == 0)
            {
                data = db.MBukus.Where(a => a.Judul == name && a.IsDelete == false).FirstOrDefault();
            }
            else
            {
                data = db.MBukus.Where(a => a.Judul == name && a.IsDelete == false && a.Id != id).FirstOrDefault();
            }

            if (data != null)
            {
                return true;
            }

            return false;
        }

        [HttpPost("Save")]
        public VMResponse Save(MBuku data)
        {
            data.CreatedBy = Iduser;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saves";

            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Faild saved" + ex.Message;

            }
            return respon;
        }

        [HttpPut("Edit")]

        public VMResponse Edit(MBuku data)
        {
            MBuku dt = db.MBukus.Where(a => a.Id == data.Id).FirstOrDefault();

            if (dt != null)
            {
                dt.Judul = data.Judul;
                dt.Penerbit = data.Penerbit;
                dt.ModifiedBy = Iduser;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success saved";

                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved" + ex.Message;


                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            return respon;
        }

        [HttpDelete("Delete/{id}/{createBy}")]

        public VMResponse Delete(int id, int createBy)
        {
            MBuku dt = db.MBukus.Where(a => a.Id == id).FirstOrDefault();
            if (dt != null)
            {
                dt.IsDelete = true;
                dt.ModifiedBy = createBy;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success deleted";

                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved" + ex.Message;


                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            return respon;


        }

    }
}
