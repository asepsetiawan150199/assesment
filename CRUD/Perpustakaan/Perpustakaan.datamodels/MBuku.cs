﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Perpustakaan.datamodels
{
    [Table("m_buku")]
    public partial class MBuku
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }
        [Column("judul")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Judul { get; set; }
        [Column("penerbit")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Penerbit { get; set; }
        [Column("tahun")]
        [StringLength(10)]
        [Unicode(false)]
        public string? Tahun { get; set; }
        [Column("sinopsis")]
        [Unicode(false)]
        public string? Sinopsis { get; set; }
        [Column("stok")]
        public int? Stok { get; set; }
        [Column("id_jenis")]
        public int? IdJenis { get; set; }
        [Column("image")]
        public byte[]? Image { get; set; }
        [Column("image_path")]
        [StringLength(255)]
        [Unicode(false)]
        public string? ImagePath { get; set; }
        [Column("created_by")]
        public long CreatedBy { get; set; }
        [Column("created_on", TypeName = "datetime")]
        public DateTime CreatedOn { get; set; }
        [Column("modified_by")]
        public long? ModifiedBy { get; set; }
        [Column("modified_on", TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        [Column("deleted_by")]
        public long? DeletedBy { get; set; }
        [Column("deleted_on", TypeName = "datetime")]
        public DateTime? DeletedOn { get; set; }
        [Column("is_delete")]
        public bool IsDelete { get; set; }
    }
}
