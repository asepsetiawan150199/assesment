﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Perpustakaan.datamodels
{
    [Table("m_jenis_buku")]
    public partial class MJenisBuku
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }
        [Column("jenis")]
        [StringLength(20)]
        [Unicode(false)]
        public string? Jenis { get; set; }
        [Column("deskripsi")]
        [Unicode(false)]
        public string? Deskripsi { get; set; }
        [Column("denda")]
        public long? Denda { get; set; }
        [Column("created_by")]
        public long CreatedBy { get; set; }
        [Column("created_on", TypeName = "datetime")]
        public DateTime CreatedOn { get; set; }
        [Column("modified_by")]
        public long? ModifiedBy { get; set; }
        [Column("modified_on", TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        [Column("deleted_by")]
        public long? DeletedBy { get; set; }
        [Column("deleted_on", TypeName = "datetime")]
        public DateTime? DeletedOn { get; set; }
        [Column("is_delete")]
        public bool IsDelete { get; set; }
    }
}
