﻿
BLQ_22();
Console.ReadKey();
static void BLQ_22()
{
    Console.WriteLine("nomer 22 tentang lilin yang meleleh");
    Console.Write("masukkan panjang lilin : ");
    string[] panjangLilin = Console.ReadLine().Trim().Split(" ");
    int[] panjangLilinInt = Array.ConvertAll(panjangLilin, int.Parse);
    int[] fibonacci = new int[panjangLilinInt.Length];
    fibonacci[0] = 0;
    fibonacci[1] = 1;

    for (int i = 0; i < fibonacci.Length; i++)
    {
        if (i >= 2)
        {
            fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
        }

    }

    bool end = true;
    do
    {
        for (int i = 0; i < panjangLilinInt.Length; i++)
        {
            panjangLilinInt[i] -= fibonacci[i];
            if (panjangLilinInt[i] <= 0)
            {
                end = false;
                Console.WriteLine($"Lilin yang habis duluan adalah lilin dengan index ke- {i}");
            }
        }
    } while (end);
}