﻿
BLQ_15();
Console.ReadKey();
static void BLQ_15()
{
    Console.WriteLine("Soal Nomor 15");
    Console.Write("Masukan Input Waktu (hh:mm:dd(PM/AM)) : ");
    string input = Console.ReadLine().Replace(" ", "").ToUpper();
    try
    {
        DateTime date1 = DateTime.ParseExact(input, "hh:mm:sstt", null);
        Console.WriteLine(date1.ToString("HH:mm:ss"));
    }
    catch (Exception e)
    {
        Console.WriteLine("Format yang dimasukkan salah");
        Console.WriteLine($"pesan eror : {e.Message}");
    }
}